# grub-gt

Ha igazi IK-s szeretnél lenni, de mégis Linux (fúj) rendszert kell használnod, akkor legalább a GRUB bakancs menüd pompázzon az inkompetencia színeiben.

# Tutorial

Igazán egyszerű:
```
sudo mkdir /boot/grub/themes
sudo cp -r grubGT /boot/grub/themes
```

majd kedvenc szövegszerkeztőddel:
```
sudo nvim /etc/default/grub
```

a fájl végére rakj egy ilyen sort:
```
GRUB_THEME=/boot/grub/themes/grubGT/theme.txt
```

Ezt követően már csak ezt futtatod:
```
sudo update-grub
```

vagy
```
grub-mkconfig -o /boot/grub/grub.cfg
```

boldogság.
